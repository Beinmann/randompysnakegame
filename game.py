from config import Cfg
from snake import Snake
from game_state_enum import GameState
from movement_enum import Movement
import random


class Game:
    def __init__(self, screen, clock):
        self.screen = screen
        self.clock = clock

        self.dt_since_last_move = 0
        self.dt = 0
        self.time_between_moves_seconds = 0.07

        self.score = 0
        self.game_state = GameState.RUNNING

        self.snake = Snake()

        self.apple_position = None
        self.spawn_new_apple()

        # Random variables (These were the variables causing trouble because I made them static but then tried to restart the game which caused weird behavior that was hard to debug. In the end I got it with pdb at least)
        self.game_over_rendered = False
        self.new_movement_dir = Movement.NONE

    def spawn_new_apple(self):
        # TODO this could cause issues when there are no free spaces left (but that shouldn't occur)

        while True:
            col = random.randint(1, Cfg.COLUMN_AMOUNT) - 1
            row = random.randint(1, Cfg.ROW_AMOUNT) - 1
            self.apple_position = (col, row)
            if self.apple_position not in self.snake.position_list:
                return
