from game_state_enum import GameState
from config import Cfg
import pygame


def update(game, events):
    for event in events:
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                game.game_state = GameState.EXIT
            if event.key == pygame.K_r:
                game.game_state = GameState.RESTART

    if not game.game_over_rendered:
        string = "GAME OVER"
        font = pygame.font.Font('FreeSansBold\\FreeSansBold.ttf', 56)
        text = font.render(string, True, (0, 0, 0))
        textRect = text.get_rect()
        textRect.center = (Cfg.WINDOW_WIDTH / 2, Cfg.WINDOW_HEIGHT / 2)
        game.screen.blit(text, textRect)

        string = "(Q to exit | R to restart)"
        font = pygame.font.Font('FreeSansBold\\FreeSansBold.ttf', 30)
        text = font.render(string, True, (0, 0, 0))
        textRect = text.get_rect()
        textRect.center = (Cfg.WINDOW_WIDTH / 2, Cfg.WINDOW_HEIGHT / 2 + 40)
        game.screen.blit(text, textRect)

        game_over_rendered = True
