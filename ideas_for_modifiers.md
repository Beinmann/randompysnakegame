# Ideas for Modifiers
 - INFO: these will be decided and then I will add these one by one

### Standard Things
 - Add obstacles
 - add wraparound on the edges
 - Add rare apples that give more points (and which maybe disappear after some time)
 - Multiple apples at the same time
 - Random starting location
 - Make the outlines of the snake more clear


### Slightly Harder things
 - Level larger than the screen and it scrolls with you
 - Multiple Levels
 - Persistent leaderboard
    - Bonus: Connected to firebase
 - Settings for the look of the game
 - A menu which lets you change settings
 - A main menu
 - Skins
    - Bonus: earn them through playing
 - More points gain
 - Alternative selectable modes
 - Credits
 - Rainbow effect
 - Debuffs
 - Reverse mode (you get smaller but faster until you win)
 - Allow going through oneself (whats the lose condition)
 - you don't get larger but instead the arena gets smaller


### Weird things
 - Apples move around
    - Bonus: They don't follow the grid
 - Enemies which attack you
 - You have a goal and have to reach it
 - A bossfight?
 - Add guns
 - Add rougelike elements
 - Meta progression in between games
 - Make it a metroidvania
 - Add a minimap
 - Add lore
 - Shrek
 - big point multiplier (x20 or higher)
    - maybe random
 - Bruh moment
 - Thick snake (2 spaces wide but could also be something else)
 - True ending
 - Rhythm game
 - Tower defense
 - Dummy thick
 - Accordion
