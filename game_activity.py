from movement_enum import Movement, reverse_movement
from game_state_enum import GameState
from config import Cfg
import pygame




def update(game, events):
    game.dt_since_last_move += game.dt

    check_if_movement_dir_changed(game, events)

    # Only call the update functions and draw everything
    # when the snake moves
    if game.dt_since_last_move > game.time_between_moves_seconds:
        game.dt_since_last_move -= game.time_between_moves_seconds
        draw_background(game)

        # Ensure the new movement dir is valid
        if game.new_movement_dir is not None:
            # If snake len is more than 1 then also disallow 180 degree turns
            did_direction_reverse = game.snake.movement_direction == reverse_movement(game.new_movement_dir)
            if game.snake.length == 1 or not did_direction_reverse:
                game.snake.movement_direction = game.new_movement_dir

        update_player_pos(game)

        check_for_collision(game)

        eat_apple_if_possible(game)

        trim_snake_to_appropriate_length(game)

        draw_apple(game)

        draw_snake(game)

        draw_score(game)


def draw_background(game):
    game.screen.fill("purple")
    for row in range(Cfg.ROW_AMOUNT):
        for col in range(Cfg.COLUMN_AMOUNT):
            x = col * Cfg.CELL_WIDTH
            y = row * Cfg.CELL_HEIGHT
            w = Cfg.CELL_WIDTH
            h = Cfg.CELL_HEIGHT
            rect = pygame.Rect(x, y, w, h)
            pygame.draw.rect(game.screen, Cfg.WHITE, rect, 1)


def check_if_movement_dir_changed(game, events):
    # Check for movement inputs
    # WASD, HJKL and arrow keys are allowed
    new_direction = None
    for event in events:
        if event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_w, pygame.K_k, pygame.K_UP]:
                new_direction = Movement.NORTH
            elif event.key in [pygame.K_s, pygame.K_j, pygame.K_DOWN]:
                new_direction = Movement.SOUTH
            elif event.key in [pygame.K_a, pygame.K_h, pygame.K_LEFT]:
                new_direction = Movement.WEST
            elif event.key in [pygame.K_d, pygame.K_l, pygame.K_RIGHT]:
                new_direction = Movement.EAST

    if new_direction is not None:
        game.new_movement_dir = new_direction



def update_player_pos(game):
    if game.snake.movement_direction == Movement.NONE:
        return

    cur_pos = game.snake.position_list[-1]
    x, y = cur_pos

    if game.snake.movement_direction == Movement.NORTH:
        y -= 1
    if game.snake.movement_direction == Movement.SOUTH:
        y += 1
    if game.snake.movement_direction == Movement.WEST:
        x -= 1
    if game.snake.movement_direction == Movement.EAST:
        x += 1

    game.snake.position_list.append((x, y))


def check_for_collision(game):
    cur_pos = game.snake.position_list[-1]
    if cur_pos in game.snake.position_list[:-1]:
        game_over(game)

    x, y = cur_pos
    if x < 0 or y < 0:
        game_over(game)
    if x >= Cfg.COLUMN_AMOUNT or y >= Cfg.ROW_AMOUNT:
        game_over(game)


def eat_apple_if_possible(game):
    cur_head_pos = game.snake.position_list[-1]
    if game.apple_position == cur_head_pos:
        game.snake.length += 1
        game.score += 1
        game.spawn_new_apple()


def trim_snake_to_appropriate_length(game):
    # TODO this should work for now but maybe make this more general for cases where the snake is more than 1 pooint longer than max
    # Remove the last element of the snake if snake is too long
    if len(game.snake.position_list) > game.snake.length:
        game.snake.position_list.pop(0)


def draw_apple(game):
    col, row = game.apple_position
    w = Cfg.CELL_WIDTH
    h = Cfg.CELL_HEIGHT
    x = col * w
    y = row * h

    rect = pygame.Rect(x, y, w, h)
    pygame.draw.rect(game.screen, Cfg.APPLE_COLOR, rect)


def draw_snake(game):
    for pos in game.snake.position_list:
        col, row = pos
        x = col * Cfg.CELL_WIDTH
        y = row * Cfg.CELL_HEIGHT
        w = Cfg.CELL_WIDTH
        h = Cfg.CELL_HEIGHT
        rect = pygame.Rect(x, y, w, h)

        pygame.draw.rect(game.screen, "red", rect)


def draw_score(game):
    string = f"Score: {game.score}"
    font = pygame.font.Font('FreeSansBold\\FreeSansBold.ttf', 30)
    text = font.render(string, True, (0, 0, 0))
    textRect = text.get_rect()
    textRect.center = (Cfg.WINDOW_WIDTH - 120, 45)
    game.screen.blit(text, textRect)


def game_over(game):
    game.game_state = GameState.GAME_OVER
