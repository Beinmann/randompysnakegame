class Movement:
    NONE = 0
    NORTH = 1
    SOUTH = 2
    WEST = 3
    EAST = 4


def reverse_movement(movement_enum):
    if movement_enum == Movement.NONE:
        return Movement.NONE
    elif movement_enum == Movement.NORTH:
        return Movement.SOUTH
    elif movement_enum == Movement.SOUTH:
        return Movement.NORTH
    elif movement_enum == Movement.WEST:
        return Movement.EAST
    elif movement_enum == Movement.EAST:
        return Movement.WEST
