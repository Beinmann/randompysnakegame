from game import Game
from game_state_enum import GameState
from config import Cfg
import game_activity
import game_over_activity
import pygame


def run():
    pygame.init()
    screen_width = Cfg.WINDOW_WIDTH
    screen_height = Cfg.WINDOW_HEIGHT

    screen = pygame.display.set_mode((screen_width, screen_height))
    clock = pygame.time.Clock()

    game = Game(screen, clock)
    while game.game_state != GameState.EXIT:
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                game.game_state = GameState.EXIT

        if game.game_state == GameState.RESTART:
            game = Game(screen, clock)
        elif game.game_state == GameState.RUNNING:
            game_activity.update(game, events)

        if game.game_state == GameState.GAME_OVER:
            game_over_activity.update(game, events)

        # flip() the display to put your work on screen
        pygame.display.flip()

        # limits FPS to 60
        # dt is delta time in seconds since last frame, used for framerate-
        # independent physics.
        game.dt = game.clock.tick(60) / 1000

    pygame.quit()
