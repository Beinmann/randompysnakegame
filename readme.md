# Random Snake Game

 - Inspired by https://www.youtube.com/watch?v=bdAfp_SqQGI
 - Create Basic Snake game
 - then continue to add random modifiers

# Goals
 - Quickly create a working prototype of a game
 - learn how to modify the game in random and potentially drastic ways while documenting the process via good git practice

