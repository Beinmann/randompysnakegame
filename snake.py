from movement_enum import Movement


class Snake:
    def __init__(self):
        self.length = 1
        self.movement_direction = Movement.NONE
        self.position_list = [(0, 0)]
